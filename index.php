<?php include_once "PrintNumbers.php"; ?>
<?php include "Square.php"; ?>
<?php include "Circle.php"; ?>
<?php
echo "--------------------Q1----------------------" . "<br />";
//Q1:
PrintNumbers::printOut(1, 100);

echo "<br />";
echo "<br />";
echo "<br />";

echo "--------------------Q2----------------------" . "<br />";

//Q2 - Test C1:
echo "--------T1---------" . "<br />";
$square = new Square();
$square->setWidth(6);
$square->setHeight(10);
$circle = new Circle();
$circle->setRadius(2);
echo "Square Area: " . $square->areaCalculation() . "<br />";
echo "Circle Area: " . $circle->areaCalculation() . "<br />";
echo "Contain number of: " . $square->containsNumberOf($circle) . "<br />";
echo "Excessive:" . $square->hasExcessiveAreaOf($circle) . "<br />";

//Q2 - Test C2:
echo "--------T2---------" . "<br />";
$square->setWidth(2);
$square->setHeight(2);
$circle->setRadius(10);
echo "Square Area: " . $square->areaCalculation() . "<br />";
echo "Circle Area: " . $circle->areaCalculation() . "<br />";
echo "Contain number of: " . $circle->containsNumberOf($square) . "<br />";
echo "Excessive:" . $circle->hasExcessiveAreaOf($square) . "<br />";