<?php
include "ShapeInterface.php";

class Circle implements ShapeInterface {

    private $radius;
    private $area;
    private $numberOfInsideShape;
    private $insideShapeArea;
    const PI = 3.14159265359;

    public function setRadius($radius)
    {
        $this->radius = $radius;
    }

    public function getRadius()
    {
        return $this->radius;
    }

    public function setArea($area)
    {
        $this->area = $area;
    }

    public function getArea()
    {
        if (!$this->area) {
            $this->areaCalculation();
        }
        return $this->area;
    }

    public function areaCalculation()
    {
        $area = $this->radius * $this->radius * $this::PI;
        $this->setArea($area);
        return $area;
    }

    public function containsNumberOf(ShapeInterface $shape)
    {
        if (!$this->insideShapeArea) {
            $this->insideShapeArea = $shape->areaCalculation();
        }
        $this->numberOfInsideShape = floor($this->getArea() / $this->insideShapeArea);
        return $this->numberOfInsideShape;
    }

    public function hasExcessiveAreaOf(ShapeInterface $shape)
    {

        $totalFilledArea = $this->insideShapeArea * $this->numberOfInsideShape;

        $excessiveArea = $this->getArea() - $totalFilledArea;
        return $excessiveArea;
    }
}