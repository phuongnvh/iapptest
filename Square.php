<?php
include "ShapeInterface.php";

class Square implements ShapeInterface {

    private $width;
    private $height;
    private $area;
    private $insideShapeArea;
    private $numberOfInsideShape;

    public function setWidth($width)
    {
        $this->width = $width;
    }

    public function getWidth()
    {
        return $this->width;
    }

    public function setHeight($height)
    {
        $this->height = $height;
    }

    public function getHeight()
    {
        return $this->height;
    }

    public function setArea($area)
    {
        $this->area = $area;
    }

    public function getArea()
    {
        if (!$this->area) {
            $this->areaCalculation();
        }
        return $this->area;
    }

    public function areaCalculation()
    {
        $area = $this->width * $this->height;
        $this->setArea($area);
        return $area;
    }

    public function containsNumberOf(ShapeInterface $shape)
    {
        if (!$this->insideShapeArea) {
            $this->insideShapeArea = $shape->areaCalculation();
        }
        $this->numberOfInsideShape = floor($this->getArea() / $this->insideShapeArea);
        return $this->numberOfInsideShape;
    }

    public function hasExcessiveAreaOf(ShapeInterface $shape)
    {

        $totalFilledArea = $this->insideShapeArea * $this->numberOfInsideShape;

        $excessiveArea = $this->getArea() - $totalFilledArea;
        return $excessiveArea;
    }
}