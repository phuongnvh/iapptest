<?php
if (!interface_exists('ShapeInterface')) {
    interface ShapeInterface
    {
        public function areaCalculation();

        public function containsNumberOf(ShapeInterface $shape);

        public function hasExcessiveAreaOf(ShapeInterface $shape);
    }
}