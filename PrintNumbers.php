<?php

/**
 * Class PrintNumbers
 */
class PrintNumbers
{
    public static function printOut($from, $to)
    {
        for ($i = $from; $i <= $to; $i++) {
            if (!($i % 15)) {
                echo " FizzBuzz ";
                continue;
            }

            if (!($i % 3)) {
                echo " Fizz ";
                continue;
            }

            if (!($i % 5)) {
                echo " Buzz ";
                continue;
            }

            echo " " . $i . " ";
        }
    }
}
